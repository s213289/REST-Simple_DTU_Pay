package com.dtu.web;

import com.dtu.web.domain.bank.BankServiceService;
import com.dtu.web.domain.bank.*;
import com.dtu.web.model.*;
import com.dtu.web.exception.PaymentException;
import com.dtu.web.model.Payment;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author ajarmolkowicz
 * @date 1/6/22
 * @project simple_rest_demo
 */
public class SimpleDTUPay {
    WebTarget baseUrl;
    BankService bankService;

    public SimpleDTUPay() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
        BankServiceService bankServiceService = new BankServiceService();
        bankService = bankServiceService.getBankServicePort();
    }

    public String registerBankAccount(String lastName, String firstName, String cprNumber, BigDecimal amount) {
        User user = new User();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setCprNumber(cprNumber);
        try {
            return bankService.createAccountWithBalance(user, amount);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String registerDTUPayAccount(User user, String accountId) {
        try {
            return baseUrl
                    .path("account")
                    .path("register")
                    .queryParam("accountId", accountId)
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public BigDecimal getAccountBalance(String accountId) {
        try {
            return bankService.getAccount(accountId).getBalance();
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean pay(Payment payment) throws PaymentException {
        return baseUrl
                .path("payments")
                .path("pay")
                .request()
                .post(Entity.entity(payment, MediaType.APPLICATION_JSON), Boolean.class);
    }

    public List<Payment> list() {
        return baseUrl
                .path("payments")
                .request(MediaType.APPLICATION_JSON)
                .get(Response.class)
                .readEntity(new GenericType<>() {
                });
    }

    public void retireAccount(String accountId) {
        try {
            bankService.retireAccount(accountId);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }
}