package com.dtu.web;

import com.dtu.web.model.Payment;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.vertx.ext.auth.User;

import java.lang.System;
import javax.ws.rs.WebApplicationException;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;


/**
 * @author ajarmolkowicz
 * @date 1/4/22
 * @project code-with-quarkus
 */
public class SimpleDTUPaySteps {
    SimpleDTUPay dtuPay = new SimpleDTUPay();
    String errorMessage;
    boolean successful;
    String customerId, customerAccountId, customerFirstName = UUID.randomUUID().toString(), customerLastName = UUID.randomUUID().toString(), customerCprNumber = UUID.randomUUID().toString();
    String merchantId, merchantAccountId, merchantFirstName = UUID.randomUUID().toString(), merchantLastName = UUID.randomUUID().toString(), merchantCprNumber = UUID.randomUUID().toString();

    @Given("a customer with a bank account with balance {}")
    public void aCustomerWithABankAccountWithBalance(BigDecimal balance) {
        customerAccountId = dtuPay.registerBankAccount(customerLastName, customerFirstName, customerCprNumber,
                balance);
    }

    @Given("a merchant with a bank account with balance {}")
    public void aMerchantWithABankAccountWithBalance(BigDecimal balance) {
        merchantAccountId = dtuPay.registerBankAccount(merchantLastName, merchantFirstName, merchantCprNumber,
                balance);
    }

    @And("that the customer is registered with DTU Pay")
    public void thatTheCustomerIsRegisteredWithDtuPay() {
        assertTrue(customerAccountId != null && !customerAccountId.isEmpty());
        com.dtu.web.domain.bank.User customer = new com.dtu.web.domain.bank.User();
        customer.setFirstName(customerFirstName);
        customer.setLastName(customerLastName);
        customer.setCprNumber(customerCprNumber);
        customerId = dtuPay.registerDTUPayAccount(customer, customerAccountId);
    }

    @And("that the merchant is registered with DTU Pay")
    public void thatTheMerchantIsRegisteredWithDtuPay() {
        assertTrue(merchantAccountId != null && !merchantAccountId.isEmpty());
        com.dtu.web.domain.bank.User merchant = new com.dtu.web.domain.bank.User();
        merchant.setFirstName(merchantFirstName);
        merchant.setLastName(merchantLastName);
        merchant.setCprNumber(merchantCprNumber);
        merchantId = dtuPay.registerDTUPayAccount(merchant, merchantAccountId);
    }

    @When("the merchant initiates a payment for {} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(BigDecimal amount) {
        try {
            this.successful = dtuPay.pay(Payment.builder().creditorId(customerId).debtorId(merchantId).amount(amount).build());
        } catch (WebApplicationException pe) {
            this.errorMessage = pe.getResponse().readEntity(String.class);
        }
    }

    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        assertTrue(successful);
    }

    @And("the balance of the customer at the bank is {} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(BigDecimal balance) {
        assertEquals(balance, dtuPay.getAccountBalance(customerAccountId));
    }

    @And("the balance of the merchant at the bank is {} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(BigDecimal balance) {
        assertEquals(balance, dtuPay.getAccountBalance(merchantAccountId));
    }

    @After()
    public void afterScenario(){
        dtuPay.retireAccount(customerAccountId);
        dtuPay.retireAccount(merchantAccountId);
        System.out.println("This will run after the Scenario");
    }
}