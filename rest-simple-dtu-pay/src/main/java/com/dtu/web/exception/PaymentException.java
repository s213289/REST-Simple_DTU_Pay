package com.dtu.web.exception;

import javax.ws.rs.WebApplicationException;

/**
 * @author ajarmolkowicz
 * @date 1/4/22
 * @project code-with-quarkus
 */
public class PaymentException extends WebApplicationException {
    public PaymentException(String message) {
        super(message);
    }
}