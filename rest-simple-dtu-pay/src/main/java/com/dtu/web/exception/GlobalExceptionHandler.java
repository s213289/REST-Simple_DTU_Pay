package com.dtu.web.exception;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author ajarmolkowicz
 * @date 1/6/22
 * @project simple_rest_demo
 */
@Provider @Slf4j
public class GlobalExceptionHandler implements ExceptionMapper<WebApplicationException> {
    @Override public Response toResponse(WebApplicationException e) {
        log.error("Exception {} thrown. Message: {}", e.getClass(), e.getMessage());
        return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type("text/plain").build();
    }
}