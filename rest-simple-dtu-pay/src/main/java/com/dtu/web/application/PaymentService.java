package com.dtu.web.application;

import com.dtu.web.domain.bank.*;
import com.dtu.web.domain.model.*;
import com.dtu.web.domain.contracts.CustomerRepository;
import com.dtu.web.domain.contracts.PaymentRepository;
import com.dtu.web.domain.model.Payment;
import com.dtu.web.exception.PaymentException;
import com.dtu.web.infrastructure.InMemoryCustomerRepository;
import com.dtu.web.infrastructure.InMemoryPaymentRepository;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * @author ajarmolkowicz
 * @date 1/6/22
 * @project code-with-quarkus
 */
@Slf4j
@ApplicationScoped
class PaymentService {
    private PaymentRepository paymentRepository = new InMemoryPaymentRepository();
    private CustomerRepository customerRepository = new InMemoryCustomerRepository();
    private BankServiceService bankServiceService = new BankServiceService();
    private BankService bankService = bankServiceService.getBankServicePort();

    String registerUser(User user, String accountId) {
        return customerRepository.addCustomer(user, accountId);
    }

    boolean registerPayment(Payment payment) {
        Customer customer;
        Customer merchant;

        if (customerRepository.find(payment.getCreditorId()).isPresent()) {
            customer = customerRepository.find(payment.getCreditorId()).get();
        } else {
            throw new PaymentException(String.format("customer with id %s is unknown", payment.getCreditorId()));
        }
        if (customerRepository.find(payment.getDebtorId()).isPresent()) {
            merchant = customerRepository.find(payment.getDebtorId()).get();
        } else {
            throw new PaymentException(String.format("merchant with id %s is unknown", payment.getDebtorId()));
        }

        try {
            bankService.transferMoneyFromTo(customer.getAccountId(), merchant.getAccountId(), payment.getAmount(), "test");
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    List<Payment> listPayments() {
        return paymentRepository.listPayments();
    }
}