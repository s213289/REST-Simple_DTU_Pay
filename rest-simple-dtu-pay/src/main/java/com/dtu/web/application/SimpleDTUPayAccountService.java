package com.dtu.web.application;

import com.dtu.web.domain.bank.User;
import com.dtu.web.domain.model.RegisterUserRequest;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.inject.Default;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Slf4j
@Path("account")
public class SimpleDTUPayAccountService {
    private final PaymentService paymentService;

    public SimpleDTUPayAccountService(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @POST
    @Path("register")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String registerUser(@NotNull User user, @QueryParam("accountId") String accountId) {
        log.info("Registering new user {}.", user.toString());
        return paymentService.registerUser(user, accountId);
    }
}
