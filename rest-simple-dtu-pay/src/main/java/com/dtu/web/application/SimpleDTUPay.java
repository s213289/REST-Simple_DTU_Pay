package com.dtu.web.application;

import com.dtu.web.domain.bank.User;
import com.dtu.web.domain.model.RegisterUserRequest;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import com.dtu.web.domain.model.Payment;
import com.dtu.web.exception.PaymentException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author ajarmolkowicz
 * @date 1/4/22
 * @project code-with-quarkus
 */
@Path("/payments") @Slf4j
public class SimpleDTUPay {
    private final PaymentService paymentService;

    public SimpleDTUPay(PaymentService paymentService) {
        this.paymentService = paymentService;
    }
    @POST
    @Path("pay")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean pay(Payment payment) throws PaymentException {
        log.info("Registering new payment {}.", payment);
        return paymentService.registerPayment(payment);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Payment> list() {
        log.info("List all payments.");
        return paymentService.listPayments();
    }
}
