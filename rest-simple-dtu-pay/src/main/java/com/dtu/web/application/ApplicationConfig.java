package com.dtu.web.application;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author ajarmolkowicz
 * @date 1/6/22
 * @project simple_rest_demo
 */
@ApplicationPath("")
public class ApplicationConfig extends Application {
}