package com.dtu.web.infrastructure;

import com.dtu.web.domain.model.Payment;
import com.dtu.web.domain.contracts.PaymentRepository;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ajarmolkowicz
 * @date 1/4/22
 * @project code-with-quarkus
 */
public class InMemoryPaymentRepository implements PaymentRepository {
    private final List<Payment> payments = Collections.synchronizedList(new LinkedList<>());

    @Override public boolean save(Payment payment) {
        return payments.add(payment);
    }

    @Override public List<Payment> listPayments() {
        return payments;
    }
}