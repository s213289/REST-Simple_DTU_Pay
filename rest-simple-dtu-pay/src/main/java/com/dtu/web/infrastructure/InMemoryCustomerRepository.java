package com.dtu.web.infrastructure;

import com.dtu.web.domain.bank.User;
import com.dtu.web.domain.contracts.CustomerRepository;
import com.dtu.web.domain.model.Customer;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @author ajarmolkowicz
 * @date 1/6/22
 * @project code-with-quarkus
 */
@Slf4j
public class InMemoryCustomerRepository implements CustomerRepository {
    private List<Customer> customers;

    public InMemoryCustomerRepository() {
        customers = new ArrayList<>();
    }

    @Override
    public Optional<Customer> find(String id) {
        return customers.stream().filter($ -> $.getId().equals(id)).findAny();
    }

    @Override
    public String addCustomer(User user, String accountId) {
        Optional<Customer> customerFromDb = customers.stream().filter($ ->
                $.getAccountId() != null && $.getAccountId().equals(accountId) &&
                        $.getCprNumber() != null && $.getCprNumber().equals(user.getCprNumber()) &&
                        $.getFirstName() != null && $.getFirstName().equals(user.getFirstName()) &&
                        $.getLastName() != null && $.getLastName().equals(user.getLastName())).findAny();
        if (customerFromDb.isPresent()) {
            return customerFromDb.get().getId();
        }
        Customer customer = new Customer().builder().id(UUID.randomUUID().toString()).firstName(user.getFirstName()).lastName(user.getLastName()).cprNumber(user.getCprNumber()).accountId(accountId).build();
        customers.add(customer);
        log.info("Size after add customer: " + customers.size());
        return customer.getId();
    }
}