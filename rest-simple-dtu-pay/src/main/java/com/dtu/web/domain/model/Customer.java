package com.dtu.web.domain.model;

import lombok.*;

import java.util.UUID;

/**
 * @author ajarmolkowicz
 * @date 1/6/22
 * @project code-with-quarkus
 */
@NoArgsConstructor @AllArgsConstructor @Getter @Builder
public class Customer {
    String id;
    String firstName;
    String lastName;
    String cprNumber;
    String accountId;
}