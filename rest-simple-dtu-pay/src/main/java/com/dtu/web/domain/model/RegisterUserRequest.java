package com.dtu.web.domain.model;

import com.dtu.web.domain.bank.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class RegisterUserRequest {
    User user;
    String accountId;
}
