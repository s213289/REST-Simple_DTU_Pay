package com.dtu.web.domain.contracts;

import com.dtu.web.domain.model.Payment;

import java.util.List;

/**
 * @author ajarmolkowicz
 * @date 1/4/22
 * @project code-with-quarkus
 */
public interface PaymentRepository {
    boolean save(Payment payment);
    List<Payment> listPayments();
}