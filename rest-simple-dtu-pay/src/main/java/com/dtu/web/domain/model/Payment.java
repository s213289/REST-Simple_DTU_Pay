package com.dtu.web.domain.model;

import lombok.*;
import java.math.BigDecimal;

/**
 * @author ajarmolkowicz
 * @date 1/4/22
 * @project code-with-quarkus
 */
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @Builder @EqualsAndHashCode @ToString
public class Payment {
    String creditorId;
    String debtorId;
    BigDecimal amount;
}