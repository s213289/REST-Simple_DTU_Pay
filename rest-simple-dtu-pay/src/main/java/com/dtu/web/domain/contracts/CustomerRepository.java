package com.dtu.web.domain.contracts;

import com.dtu.web.domain.bank.User;
import com.dtu.web.domain.model.Customer;

import java.util.Optional;
import java.util.UUID;

/**
 * @author ajarmolkowicz
 * @date 1/6/22
 * @project code-with-quarkus
 */
public interface CustomerRepository {
    Optional<Customer> find(String id);
    String addCustomer(User user, String accountId);
}
